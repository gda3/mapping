mapping out the Digital Media environment in Arnhem by 3rd years GDA
--

Documentation for software involved
--

Open Streetmaps - http://wiki.openstreetmap.org/wiki
 
HTML - https://www.w3schools.com/

Leaflet.js - http://leafletjs.com/reference.html

Git - https://git-scm.com/doc

Git tutorials - [These tutorials](https://www.youtube.com/playlist?list=PL-osiE80TeTuRUfjRe54Eea17-YfnOOAx) by [Corey Shafer](http://coreyms.com) are quite ok

Git Workflow
--
a brief documentation on how to work on the map project with git.  


first copy it from the server to your computer using git clone:

`git clone https://gitlab.com/gda3/mapping.git`


verify you are on the branch you want to be, the currently used one will turn up green:

`git branch`


switch to work on your branch, either 'o_g4ng' or 'middag' depending on what group you are in:

`git checkout middag`


If you want to switch back-and forth just use checkout again:

`git checkout master`


make sure you are up to date:

`git pull`
	

see what other people change in the mean time:

`git log`


if you are up to date, you can make some changes to your files

if you've added some changes verify that things:

`git status`


you should see some file names in read indicating that they are modified but not yet 'staged'. Add them to staging:

`git add 'filename' 'filename2'`  


verify your files are 'staged' and ready to be committed, they will show up in green if you do:

`git status`


commit your changes to your git repository by using:

`git commit -m 'I made some changes to these files and here I describe what those changes are'`
	
verify that you managed to commit your changes by seeing the change log:

`git log`
	
add your changes to the server:

`git push`


Git commands visualized 
--
The infamous drawing, newest version. 
![git commands visualized](https://xmpp.roelof.info:5281/upload/b698b422-14e7-4757-aa31-b161ab535071/git_commands.jpg)


Oh NO!!
--

a quick reference guide to correcting things that went wrong...

Updates were rejected because the remote contains work that you do not have locally
----

So you tried to push your changes but you get something like this?

    To https://gitlab.com/gda3/mapping.git
     ! [rejected]        master -> master (fetch first)
    error: failed to push some refs to 'https://gitlab.com/gda3/mapping.git'
    hint: Updates were rejected because the remote contains work that you do
    hint: not have locally. This is usually caused by another repository pushing
    hint: to the same ref. You may want to first integrate the remote changes
    hint: (e.g., 'git pull ...') before pushing again.
    hint: See the 'Note about fast-forwards' in 'git push --help' for details.

This means that someone changed a file on the server, while you where editing it locally. To fix this you need to 'merge' the two versions of the file.
If you're lucky a 'pull' is enough because git is quite good at merging files:

`git pull`

You will probably get an interface that asks you to add a commit message for your merge. It will show you a text editor that can be difficult to escape from if you don't know what you are doing:
You can exit this interface by using ":wq" if it is 'vi' or "ctrl+x" if it is 'nano'

After you've escaped this interface, you have merged the remote version with your local one so now try to push again:

`git push`


I accidentally pushed a file to the server that shouldn't be there at all!!1
----

So you've managed to upload a file that shouldn't be there. There is a way to erase it from all our git history but be carefull, this is the nuclear option. No ctrl-z or git checkout will save you from this!

Erase a file called `myfile` from our git history:

    git filter-branch --force --index-filter \
    'git rm --cached --ignore-unmatch myfile' \
    --prune-empty --tag-name-filter cat -- --all
    

![it's gone](https://upload.wikimedia.org/wikipedia/commons/7/79/Operation_Upshot-Knothole_-_Badger_001.jpg)
