// this is our code which talks to the leaflet libraries.
// for documentation about leaflet: http://leafletjs.com/reference.html

//initiate a map on the '#map' layer and set the initial views.
var map = L.map( 'map', {
    center: [51.98510340000001, 5.898729600000024],
    minZoom: 11,
    zoom: 14
}); 

// then we add a layer of map tiles, these are the image fragments that make up the total draggable/zoomable 'slippy map'.
L.tileLayer( 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    subdomains: ['a','b','c']
}).addTo( map );

// this is a small iterator to go through all the points we made in poi.json and add them as markers to our map.
// with the .bindPopup method we can display more information about each point
for ( var i=0; i < markers.length; ++i ){
 L.marker( [markers[i].lat, markers[i].lng] )
  .bindPopup( '<a href="' + markers[i].url + '" target="_blank">' + markers[i].name + '</a>' )
  .addTo( map );
}
